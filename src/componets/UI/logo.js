

import React from 'react'
import {Link} from 'react-router-dom';

import cart from '../../resources/logo/cart.png';



export const Logo = (props) =>{
   
    const template =  <img src={cart} 
                            style={{ width:props.width,
                                        heigh:props.height,borderRadius:props.radius}}/>
                                   

    if(props.link) {
         return(<Link to={props.linkTo}>{template}</Link>)     
    }
    else {
        return template;
    }

}