import React, { Component } from 'react'
import classes from './Login.css'
import axios from 'axios';
import SignUp from '../SignUp/SignUp';
import {
 
  FormGroup,

  FormControl,
  Button
} from "react-bootstrap";

 class Login extends Component {

state ={
  name:"",
  email:"",
  password:"",
  login_error: false,
  error_message :"",
  formType:'login',
  isLoding:false
}



    handleSubmit =  (event) =>{
      // console.log(this.state)
      event.preventDefault();
      this.setState({
        isLoding:true
      })
   
      axios({
        method : "POST",
        url : 'https://mk-api.herokuapp.com/resume/auth/login',
      data : {
        "email":this.state.email,
        "password":this.state.password

        // "email":"a@gmail.com",
        // "password":"123456"
           
        },
        headers : {
                "Content-Type" : "application/json",
               
        }
    }).then( response =>{
      this.setState({
        isLoding:false
      })
      // console.log('resp',response.data.token)
      localStorage.setItem('login_status', JSON.stringify(response.data.token));
      if(response.data.success)
      {
        this.props.history.push(
          {
              pathname:'/ProductList',
              })
      }else {
        console.log('sorry somethng went wrong')
      }
       

    } ).catch((error) => {
      if (error.response) {
        this.setState({
          login_error :true,
          error_message:'Email or Password is Wrong'
        })
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
    });

   

  
    }

    formChangeHandler = (type) =>{
      this.setState({
        formType :type
      })
    }


    

  render() {
    console.log('sad',this.state.isLoding) 
    return (
      <div className={classes.container}>
      <div className={classes.LoginCard}>
      <div className={classes.ButtonsConatiner}>
      <div className={classes.ButtonsOneStyle}>

    
      <Button
            block
           
            value="Submit"
            type="submit"
            variant="light"
            onClick={()=>this.formChangeHandler('login')}
          >
            Login
          </Button>
          </div>

          <div className={classes.ButtonsTwoStyle}>
          <Button
            block
            
            value="Submit"
            type="submit" 
            variant="light"
            onClick={()=>this.formChangeHandler('sign')}>
            Sign In
          </Button>
          </div>
      </div>
     {this.state.formType === 'login' ?  <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email"  style={{marginTop:'10%',marginLeft:'10%'}}>
            <h4>Email</h4>
            <FormControl
              autoFocus
              type="email"
             style={{width:'80%'}}
             value={this.state.email || ''} 
             onChange={(e)=>this.setState({email:e.target.value})}
            />
          </FormGroup>
          <FormGroup controlId="password"  style={{marginLeft:'10%'}}>
            <h4 style={{color:'black'}}>Password</h4>
            <FormControl
           type="password"
           style={{width:'80%'}}
           value={this.state.password} 
           onChange={(e)=>this.setState({password:e.target.value})}
            />
          </FormGroup>
         
      {this.state.isLoding ?<div style={{marginLeft:'35%'}}>
        <img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" 
        style={{heigh:'40%',width:'40%'}}/>
        </div > :<Button
            block
            
            value="Submit"
            type="submit"
            style={{backgroundColor:'#E8EAF6',boxSizing:'content-box',color:'#212121',width:'50%',marginLeft:'20%'}}
          >
            Login
          </Button>}     
      
        </form>:
        <SignUp/>}
         
        <div>{this.state.login_error ? <p>{this.state.error_message}</p> :null}</div>
      </div>
      
      </div>
    )
  }
}

export default Login;
