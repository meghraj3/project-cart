import React, { Component } from 'react'


import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/core/styles';

import SideDrawer from '../HOC/SideDrawer/SideDrawer'
import {NavLink} from 'react-router-dom';
import {Logo} from '../UI/logo';
import {
 
 Card
} from "react-bootstrap";




const styles = {
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
  };

export class Navbar extends Component {

  state = {
    opens: false,
    modalOpen:false
    
  
  };
  

  drawerHandler = (val) => {
    this.setState({ 
      opens: val 
    });
  }

  logout = () => {
    localStorage.clear();
  }

  //modal open close

  openModalHandler = (val) => {
    // console.log('modal',val)

    this.setState({
      modalOpen : !val 
    })
  }

 
  render() {
    // console.log('modal',this.state.modal_open)
    const { classes } = this.props;
    return (
     

      
     <div>

         {this.state.modalOpen ? <div style={classes.modalStyle}> <Card style={{height:'10%',width:'5%',borderRadius:'100%',position:'absolute',zIndex:12,backgroundColor:'red',left:'40%',top:'5%'}}>
       
       </Card> </div>:null} 
     
         
        <AppBar position="static" style={{width:'100%',backgroundColor:this.state.modalOpen ? 'blue' :'yellow'}}>
        <Toolbar style={{display:'flex'}}>
          
       <div style={{flexGrow:1}}>
         <Logo 
            link={true}
            linkTo="/ProjectList" 
            width="48px" 
            height="50px"
            radius="90%"
            />
       </div>
            
       <Button color="inherit" onClick={()=>this.openModalHandler(this.state.modalOpen)}>Open</Button>
          <NavLink to='/AddProject' style={{ textDecoration: 'none',color:'#FFF' }}>
          <Button color="inherit">Add Project</Button> </NavLink>
          <NavLink to='/' style={{ textDecoration: 'none',color:'#FFF' }}>
          <Button color="inherit" onClick={()=>{this.logout()}}>Logout</Button> </NavLink>
        </Toolbar>
      </AppBar>
    {/* <SideDrawer open={this.state.opens} navfun={this.drawerHandler}/> */}
   
  
    </div>
   
    
    )
  }
}

Navbar.propTypes = {
    classes: PropTypes.object.isRequired,
  };

// export default Navbar
export default withStyles(styles)(Navbar);
