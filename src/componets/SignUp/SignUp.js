import React, { Component } from 'react'
import classes from './SignUp.css'
import axios from 'axios';

import {
 
  FormGroup,
  
  FormControl,
  Button
} from "react-bootstrap";

 class SignUp extends Component {

state ={
  name :'',
  email:'',
  password:'',
  sgin_error :'',
  error_message:'',
  isLoding:false
}



SignhandleSubmit =(event) =>{
      event.preventDefault();
      this.setState({
        isLoding:true
      })
      axios({
        method: 'post',
        url: 'https://mk-api.herokuapp.com/resume/auth/register',
        data: {
          "name": this.state.name,
          "email": this.state.email,
          "password":this.state.password
        } ,
        headers : {
                "Content-Type" : "application/json",
               
        }
      }).then( response =>{
     
        this.setState({
          isLoding:false
        })
        // console.log('resp',response.data)
        // this.props.history.push('/')
       
       
         
  
      } ).catch((error) => {
        if (error.response) {
          this.setState({
            sgin_error :false,
            error_message:error.response.data.email,
            isLoding:false
          })
          console.log(error.response.data);
          // console.log(error.response.status);
          // console.log(error.response.headers);
        }
      });
    }

  render() {
    return (
      <div className={classes.container}>
      
    
      
        <form onSubmit={this.SignhandleSubmit}>
           <FormGroup controlId="email"  style={{marginTop:'10%',marginLeft:'10%'}}>
            <h4>Name</h4>
            <FormControl
              autoFocus
              type="text"
             style={{width:'80%'}}
             value={this.state.name || ''} 
              onChange={(e)=>this.setState({name:e.target.value})}
            />
          </FormGroup>
          <FormGroup controlId="password" style={{marginLeft:'10%'}}>
            <h4 style={{color:'black'}}>Email</h4>
            <FormControl
           type="email"
           style={{width:'80%'}}
           value={this.state.email} onChange={(e)=>this.setState({email:e.target.value})}
            />
          </FormGroup>
          <FormGroup controlId="password"  style={{marginLeft:'10%'}}>
            <h4 style={{color:'black'}}>Password</h4>
            <FormControl
           type="password"
           style={{width:'80%'}}
           value={this.state.password} 
           onChange={(e)=>this.setState({password:e.target.value})}
            />
          </FormGroup>
          {this.state.isLoding ?<div style={{marginLeft:'35%'}}>
        <img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" 
        style={{heigh:'20%',width:'40%'}}/>
        </div>:<Button
            block
          
            value="Submit"
            type="submit"
            style={{backgroundColor:'#E8EAF6',boxSizing:'content-box',color:'#212121',width:'50%',marginLeft:'20%'}}
          >
            Sign In
    </Button> }
         
    <h3>{this.state.error_message}</h3>
        </form>


         
       
      
   
      </div>
      
   
    )
  }
}

export default SignUp;
