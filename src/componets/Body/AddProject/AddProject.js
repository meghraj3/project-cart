import React from 'react';
import axios from 'axios';

import {
 Modal,
  Button,
  Form,
  Card
} from "react-bootstrap";


class AddProduct extends React.Component {


    state = {
        name: 'Cat in the Hat',
        description: '',
        image: 'Controlled',
        link: 'EUR',
        technology:'CSS',
        team :2,
        show:false,
        error_message:''
      };
    

    
      handleSubmit =(event) =>{
        let login_data=JSON.parse(localStorage.getItem('login_status'));
        console.log('add token',login_data)
        event.preventDefault();
        axios({
          method: 'post',
          url: 'https://mk-api.herokuapp.com/resume/project',
          data: {
            "name": this.state.name,
            "description": this.state.description,
            "image":this.state.image,
            "link": this.state.link,
            "technology": this.state.technology,
            "team":this.state.team
          } ,
          headers : {
                    "Authorization":login_data,
                    "Content-Type" : "application/json",
                 
          }
        }).then( response =>{
          
          console.log('resp',response.data)
          if(response.data!=null){
this.setState({
  show:true,
  error_message:"Project Added Succefully!"
})
          }else {
            this.setState({
              error_message:"Data Not Found"
            })
          }
         
         
           
    
        } ).catch((error) => {
          if (error.response) {
            this.setState({
              sgin_error :true,
              error_message:'Something Went Wrong',
              show:true
            })
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          }
        });
      }
      handleClose =() =>{
        this.setState({
          show:false
        })

        this.props.history.push('/ProductList')
      }
    
  render() {
  
   
  

    return (
      
<>
<Card style={{ height:'100%',width:'50%',marginLeft:'20%',marginTop:'5%',marginBottom:'5%'}}>
  <Card.Body>
   
  
  <Form onSubmit={this.handleSubmit}>
<Form.Group controlId="formBasicEmail">
  <Form.Label>Project Name</Form.Label>
  <Form.Control type="name" placeholder="Enter Project Name"  
  value={this.state.name}
  onChange={(e)=>this.setState({name:e.target.value})}/>
  
</Form.Group>

<Form.Group controlId="formBasicPassword">
  <Form.Label>Desciption</Form.Label>
  <Form.Control type="text" placeholder="description"  
  value={this.state.description}
  onChange={(e)=>this.setState({description:e.target.value})}/>
</Form.Group>

<Form.Group controlId="formBasicChecbox">
<Form.Label>Image</Form.Label>
<Form.Control type="text" placeholder="image"  
value={this.state.image}
onChange={(e)=>this.setState({image:e.target.value})}/>
</Form.Group>


<Form.Group controlId="formBasicChecbox">
<Form.Label>Link</Form.Label>
<Form.Control type="text" placeholder="link"  
value={this.state.link}
onChange={(e)=>this.setState({link:e.target.value})}/>
</Form.Group>

<Form.Group controlId="formBasicChecbox">
<Form.Label>Technology</Form.Label>
<Form.Control type="text" placeholder="technology"  
value={this.state.technology}
onChange={(e)=>this.setState({technology:e.target.value})}/>
</Form.Group>


<Form.Group controlId="formBasicChecbox">
<Form.Label>Team</Form.Label>
<Form.Control type="text" placeholder="Team"  
value={this.state.team}
onChange={(e)=>this.setState({team:e.target.value})}/>
</Form.Group>


<Button variant="primary" type="submit">
  Submit
</Button>
</Form>
   
  </Card.Body>

 
</Card>
<Modal show={this.state.show} onHide={this.handleClose}>
          
          <Modal.Body>{this.state.error_message}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
            
          </Modal.Footer>
        </Modal>
</>
    

    );
  }
}




export default AddProduct;