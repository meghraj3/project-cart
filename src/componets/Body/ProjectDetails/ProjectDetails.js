import React, { Component } from 'react'
import classes from './ProjectDetails.css'


import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {
 Button,

} from "react-bootstrap";


export class ProductDetails extends Component {

state ={
  formType:'display'
}

  changeUpdateForm = ()=>{
    this.props.history.push(
      {
          pathname:'/ProjectEdit',
          state:{
              item:this.props.location.state.item
                  }})
  }
  
  render() {
  
console.log(this.props)

    
   
    const {item} =this.props.location.state;
    // console.log('details',slideImages[0])
    return (
      <div className={classes.container}>
          <div className={classes.produtcContainer}>
               <div className={classes.produtcImage}>
                 
         <img src={item.image} alt="boohoo" className={classes.cardImage}/>

         
        
 
         </div>

              <div className={classes.produtinfo}>
              <Button variant="primary" onClick={()=>this.changeUpdateForm(this.state.formType)} style={{borderRadius:'100%',width:'5%',justifyContent:'center',alignItems:'center',marginLeft:'90%',marginTop:'2%'}}>
              <i className="fas fa-edit"></i>
</Button>
 <CardContent>
<Typography gutterBottom variant="h5">
{item.name}
</Typography>
<Typography gutterBottom variant="h5" component="p">
Technologies :{item.technology}
</Typography>
<Typography component="h2">
 {item.description}
</Typography>
<Typography component="h2">
 Team Player : {item.team}
</Typography>
</CardContent>


          
    
     
   
              </div>
          </div>
             




      </div>
    )
  }
}

export default ProductDetails
