import React, { Component } from 'react'

import classes from './ProjectList.css'
import ProductMap from './ProductMap';
import axios from 'axios';
// import {withRouter} from 'react-router-dom';
import {
    Dropdown,

  } from "react-bootstrap";



export class ProjectList extends Component {

    constructor(props){

        super(props)
        this.filter_product =[]

    }

   state = {
    product_list: [],
    product :[]
}




    openProductHandler = (item) => {
        // console.log('asd',item)
        this.props.history.push(
            {
                pathname:'/ProjectDetails',
                state:{
                    item:item
                        }})
    }
 



componentDidMount(){
    // var self = this;
    axios.get('https://mk-api.herokuapp.com/resume/project/all')
    .then((response)=> {
    //   console.log('product list',response.data);
    this.setState({
        product_list: response.data,
        product:response.data,
    })
     
    })
    .catch(function (error) {
      console.log(error);
    });
}
    dropeHandler = (val) => {
    
   if(val === '4'){
   
    // for(let i=4;i<=this.state.product_list.length;i++)
    // {
        this.filter_product =this.state.product_list.slice(0,4)
      
    // }
    // console.log('filter',this.filter_product)
    this.setState({
        product: this.filter_product
    })
   
   } else  if(val === '8'){
    this.filter_product =this.state.product_list.slice(0,8)
    this.setState({
        product: this.filter_product
    })
   } else {
    this.filter_product =this.state.product_list
    this.setState({
        product: this.filter_product
    })
   }
   console.log('filter',this.filter_product)
}


  render() {
    //   console.log('produsadas', this.state.product )
   return (
       <div className={classes.container}>
       <Dropdown>
  <Dropdown.Toggle variant="success" id="dropdown-basic" >
    Dropdown Button
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item onClick={()=>{this.dropeHandler('4')}}>show 4 items</Dropdown.Item>
    <Dropdown.Item onClick={()=>{this.dropeHandler('8')}}>show 8 items</Dropdown.Item>
    <Dropdown.Item onClick={()=>{this.dropeHandler('All')}}>Show All</Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>
       <div className={classes.row} >


           {
          this.state.product.map((item, i)=> <ProductMap item={item} key={i} ProductHandler={this.openProductHandler}/>)
         }
            
        
            {/* {this.mapHandler} */}
        
        
           
         
           

            

           
           
           
           
           
           
           
           
           </div>
      
       
       
       
       </div>
   )
}

}

export default ProjectList;
