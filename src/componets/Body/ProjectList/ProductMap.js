import React, { Component } from 'react'
import classes from './ProjectList.css'

export default class ProductMap extends Component {
  render() {
      console.log('map data',this.props.item)
    return (
    
        <div  className={classes.rowCards} onClick={()=>this.props.ProductHandler(this.props.item)}>
        <div >
          <img src={this.props.item.image} alt="boohoo" className={classes.cardImage}/>
        </div >

        <div className={classes.cardInfo}>
            {this.props.item.name}
        </div>
</div>



    )
  }
}
