import React, { Component } from 'react';

// import './App.css';
import {Route,Switch,Redirect} from 'react-router-dom';
import Layout from  './componets/HOC/Layout/Layout';
import ProjectList from './componets/Body/ProjectList/ProjectList';
import ProjectDetails from './componets/Body/ProjectDetails/ProjectDetails';
import Navbar from './componets/Navbar/Navbar';
// import Test from './componets/Test/Test';
import Login from './componets/Login/Login';
// import SignUp from './componets/SignUp/SignUp';
import AddProject from './componets/Body/AddProject/AddProject';
import ProjectEdit from './componets/Body/ProjectEdit/ProjectEdit';
class App extends Component {

 
 


  render() {
    let login_data=localStorage.getItem('login_status');
    let routes = (
      <Layout>
     <Switch>
 <Route exact path="/" component={Login} />
      
      <Redirect to="/" />
     </Switch>
     </Layout>
    )


   
    if (login_data) {
      routes = (
        <Layout>
           <Navbar /> 
        <Switch>
          <Route exact path="/ProjectList" component={ProjectList} />
        <Route exact path="/AddProject" component={AddProject} /> 
       <Route exact path="/ProjectDetails" component={ProjectDetails} /> 
        <Route exact path="/ProjectEdit" component={ProjectEdit} /> 
        
          <Redirect to="/ProjectList" />

        </Switch>
        </Layout>
      )
    }
    return (
      <div>
      
      
       
        <Switch>
     {routes}
        
        </Switch>
       

      </div>
    
    
    

    
    );
  }
}

export default App;
